<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("login");
});

Route::get('/home', function () {
    return view("home");
});

Route::get('/matapelajaran', function () {
    return view("mata_pelajaran/index");
});

Route::get('/guru ', "GuruController@index");
Route::get('/guru/tambah', function () {
    return view("guru/create");
});
Route::get('/guru/{guru} ', "GuruController@show");
Route::post('/guru ', "GuruController@index");
Route::post('/guru/edit ', "GuruController@update");
Route::get('/guru/tambah', function () {
    return view("guru/create");
});
Route::post('/guru/tambah ', "GuruController@store");
Route::get('/guru/hapus/{id} ', "GuruController@destroy");
// untuk sorting
Route::get('/guru/sort/{sort}', "GuruController@sorting");

Route::get('/penilaian', function () {
    return view("penilaian/index");
});

Route::get('/siswa', "SiswaController@index")->middleware('auth');

Route::get('/siswa/create', function () {
    return view('siswa/create');
});

Route::post('/siswa/store', 'SiswaController@store');

Route::get('/siswa/edit/{nisn}', 'SiswaController@edit');

Route::post('/siswa/update/{nisn}', 'SiswaController@update');

Route::post('/siswa', 'SiswaController@index');

Route::get('/siswa/destroy/{nisn}', 'SiswaController@destroy');

Route::get('/siswa/{sort}', 'SiswaController@sorting');

Route::post('/siswa/update/{nisn}', 'SiswaController@update');

Route::get('/siswa/destroy/{nisn}', 'SiswaController@destroy');

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false,
]);

Route::get('/home', 'HomeController@index')->name('home');
