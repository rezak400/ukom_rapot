<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToNilais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilais', function (Blueprint $table) {
            $table->foreign('siswa_nisn')->on('siswas')->references('nisn')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_guru')->on('gurus')->references('id')->onUpdate('cascade');
            $table->foreign('kd_kode')->on('kompetensi_dasars')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_an')->on('aspek_nilais')->references('id')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilais', function (Blueprint $table) {
            //
        });
    }
}
