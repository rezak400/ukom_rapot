<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->char('nisn',10)->primary();
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_kk');
            $table->string('siswa_nama');
            $table->string('siswa_alamat');
            $table->string('siswa_tanggal_lahir');
            $table->string('siswa_foto')->nullable();
            $table->string('siswa_kelas')->nullable();
            $table->string('siswa_telpon')->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
