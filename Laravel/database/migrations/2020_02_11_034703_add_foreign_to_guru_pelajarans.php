<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToGuruPelajarans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guru_pelajarans', function (Blueprint $table) {
            $table->foreign('id_guru')->on('gurus')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_mp')->on('mata_pelajarans')->references('id')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guru_pelajarans', function (Blueprint $table) {
            //
        });
    }
}
