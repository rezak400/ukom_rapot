<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    {{-- link ke bootstrap --}}
    <link rel="stylesheet" href="{{ asset("vendor/bootstrap.min.css")}}">

    {{-- link ke fontawesome --}}
    <link rel="stylesheet" href="{{ asset("vendor/font-awesome/font-awesome/css/all.css")}}">

    <link rel="stylesheet" href="{{ asset("css/custom.css")}}">

    <style>
        * {

        }
        body {
            background: #f1f1f1;
        }
        .color-primary {
            background-color: #432fc2;
        }
        .sidebar {
            height: 150vh;
        }
        .topbar {
            height: 50px;
            background-color: #432fc2;
        }
        h5 {
            font-size: 1.2vw;
        }
    </style>

{{-- link custom css --}}
<link rel="stylesheet" href="{{ asset("css/custom.css")}}">


</head>
<body>
    <div class="wrapper">
        <div class="row" style=" margin-right: 15px;">
            <div class="col">
                <div class="sidebar">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 py-2">
                                <img src="{{ asset('img/logo-smk.png') }}" class="mb-2" width="30" alt="">
                                <h4 class="font-weight-bold text-white text-center mt-2 d-inline-block">Administrator</h4>
                            </div>
                            <div class="container-fluid mt-5">
                                    <div class="row mb-2 mt-3">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-home text-white"></h5>
                                            <a href="{{ url("/home")}}"><h5 class="text-white d-inline-block">&nbsp;Dashboard</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2 mt-5">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-list text-white"></h5>
                                            <a href="{{ url("/guru")}}"><h5 class="text-white d-inline-block">&nbsp;Bidang Studi</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-book text-white"></h5>
                                            <a href="{{ url("/guru")}}"><h5 class="text-white d-inline-block">&nbsp;&nbsp;Kompetensi</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-suitcase text-white"></h5>
                                            <a href="{{ url("/guru")}}"><h5 class="text-white d-inline-block">&nbsp;Mata Pelajaran</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-list text-white"></h5>
                                            <a href="{{ url("/guru")}}"><h5 class="text-white d-inline-block">&nbsp;Kompetensi dasar</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-users text-white"></h5>
                                            <a href="{{ url("/siswa")}}"><h5 class="text-white d-inline-block">&nbsp;Siswa</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-edit text-white"></h5>
                                            <a href="{{ url("/penilaian")}}"><h5 class="text-white d-inline-block">&nbsp;Penilaian</h5></a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="nav-link">
                                            <h5 class="d-inline-block fa fa-users text-white"></h5>
                                            <a href="{{ url("/guru")}}"><h5 class="text-white d-inline-block">&nbsp;Guru</h5></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-10 p-0">
                <div class="content">
                    <div class="row"    >
                        <div class="topbar color-primary w-100">
                            <div class="row">
                                {{-- <div class="col-5"></div>
                                <div class="col-3">
                                    <div class="dropdown float-right">
                                        <a class="btn text-white dropdown-toggle font-weight-bold" href="dropdowns.htm#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Administrator
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="dropdowns.htm#">Action</a>
                                            <a class="dropdown-item" href="dropdowns.htm#">Another action</a>
                                            <a class="dropdown-item" href="dropdowns.htm#">Something else here</a>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    @yield('isi')
                </div>
            </div>
        </div>
    </div>

    {{-- <script src="{{ asset()}}"></script> --}}
</body>
</html>
