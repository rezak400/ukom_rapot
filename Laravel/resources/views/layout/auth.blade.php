<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('vendor/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/component.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/universal.css') }}">

    <link rel="shortcut icon" href="{{ asset('img/LogoBaru.png') }}" type="image/x-icon">
</head>

<body>
    <div class="container-login d-flex align-items-center">
        @yield('content')
    </div>
</body>

</html>
