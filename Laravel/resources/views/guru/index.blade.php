@extends('layout/sidebar')
@section('isi')
<div class="container-fluid">
    <div class="row pt-4 pb-3">
        <div class="col">
            <h3 class=""><i class="fa fa-users"></i>&nbsp; Data Guru</h3>
        </div>
    </div>
    <div class="row pb-4">
        <div class="col">
            <form action="{{ url("/guru")}}" class="form-inline" method="post">
                @csrf
                <input class="form-control w-75 mr-3" type="text" placeholder="Search..." name="search">
                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <div class="col-2 ">
            <select class="form-control d-inline-block" name="sorting" onchange="window.location.href = '/guru/sort/'+value">
                <option value="#" selected>Urutkan</option>
                <option value="asc" @if(Request::url(2) == 'http://127.0.0.1:3000/guru/sort/asc') selected @endif>Dari A-Z</option>
                <option value="desc" @if(Request::url(2) == 'http://127.0.0.1:3000/guru/sort/desc') selected @endif>Dari Z-A</option>
            </select>
        </div>
        <div class="col">
            <a class="btn btn-block btn-success font-weight-bold w-50 float-right" href="{{ url("/guru/tambah")}}">Tambah <i class="fa fa-plus-circle"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-striped text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Nomor Induk</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Telpon</th>
                        <th scope="col">Aksi</th>
                      </tr>
                </thead>
                <tbody>
                    @foreach ($query as $q)
                    <tr>
                        <th scope="row">{{ $loop->iteration}}</th>
                        {{-- <td>{{ $q->guru_foto}}</td> --}}
                        <td><img src="{{ asset("/uploads/".$q->guru_foto)}}" alt="" width="100"></td>
                        <td>{{ $q->guru_nama}}</td>
                        <td>{{ $q->guru_nip}}</td>
                        <td>{{ $q->guru_alamat}}</td>
                        <td>{{ $q->guru_telpon}}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ url("/guru/".$q->id)}}" role="button"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger" onclick="return confirm('yakin dihapus?')" href="{{ url("/guru/hapus/".$q->id)}}"><i class="fa fa-trash-alt"></i></a>
                        </td>
                    
                    </tr>  
                    @endforeach
                
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection

