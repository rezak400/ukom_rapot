@extends('layout/sidebar')
@section('isi')
<div class="container-fluid">
   <div class="row">
       <div class="col-12 pt-4">
           <div class="content bg-white rounded">
            <div class="row">
                <div class="col-12 pl-5 py-4 " >
                    <h3 class="fa fa-users d-inline-block"></h3>&nbsp;<h3 class="d-inline-block font-weight-bold">Tambah Guru</h3>
                </div>
            </div>
            <div class="row px-5">
                <div class="col">
                @if (session()->has("alert"))
                    <script>alert("Tambah Data Berhasil")</script>
                @endif
                <form action="{{ url("/guru/tambah")}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-4">
                            <div class="col">
                                <label for="">Nama Lengkap</label>
                                <input type="text" class="form-control" name="guru_nama" required>
                            </div>
                            <div class="col">
                                <label for="">Nomor Induk Pegawai (NIP)</label>
                                <input type="text" class="form-control" name="guru_nip" required>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username" required>
                            </div>
                            <div class="col">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="row mb-4">
                            {{-- <div class="col">
                                <label for="">NISN</label>
                                <input type="text" class="form-control">
                            </div> --}}
                            <div class="col">
                                <label for="">Email Address</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                            <div class="col">
                                <label for="">No Hp</label>
                                <input type="text" class="form-control" name="guru_telpon">
                            </div>
                            <div class="col">
                                <label for="">Foto</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                  </div>
                                  <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"  />
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            {{-- <div class="col">
                                  <label for="">Foto</label>
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />
                                      <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    </div>
                                  </div>
                            </div> --}}
                            <div class="col">
                                <label for="">Alamat</label>
                                <input type="text" class="form-control" name="guru_alamat">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col mt-3">
                                <button class="btn btn-success btn-block btn-lg">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
           </div>
       </div>
   </div>
</div>
@endsection