@extends('layout/sidebar')
@section('isi')
    <div class="container-fluid pt-4 home">
        <h1>Dashboard</h1>
        <div class="row ">
            {{-- @dump($data) --}}
            {{-- @for ($i = 0; $i < 9; $i++)
                <div class="col-4 mt-5">
                    <div class="box py-3 pt-4 rounded">
                        <div class="row justify-content-center">
                            <div class="col text-center">
                                <div class="logo">
                                    <i style="font-size : 108px; color:grey;"class="fa fa-users"></i>
                                </div>
                            </div>
                        </div>
                        <div class="title text-center mt-2">
                            <h1>50 siswa</h1>
                        </div>
                    </div>
                </div>
            @endfor --}}
            @foreach ($data as $d)
            <div class="col-4 mt-5">
                <div class="box py-3 pt-4 rounded">
                    <div class="row justify-content-center">
                        <div class="col text-center">
                            <div class="logo">
                                <i style="font-size : 108px; color:grey;"class="fa fa-users"></i>
                            </div>
                        </div>
                    </div>
                    <div class="title text-center mt-2">
                    <h1>{{$d["jumlah"]}}  {{$d["nama"]}}</h1>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
