@extends('layout/sidebar')

@section('isi')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pt-4">
                <div class="content bg-white rounded" style=" height: 115vh;">
                    <div class="row">
                        <div class="col-12 pl-5 py-4 " >
                            <h3 class="fa fa-users d-inline-block"></h3>&nbsp;<h3 class="d-inline-block font-weight-bold">Tambah Murid</h3>
                        </div>
                    </div>
                    <div class="row px-5">
                        <div class="col">
                            <form action="{{ url('/siswa/store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Nama</label>
                                        <input type="text" name="nama" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Alamat</label>
                                        <input type="text" name="alamat" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Kelas</label>
                                        <input type="text" name="kelas" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">NISN</label>
                                        <input type="text" name="nisn" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Telpon</label>
                                        <input type="text" name="telpon" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                          <label for="">Foto</label>
                                          <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                              {{--

                                                <input type="file" name="foto" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />

                                                Debugging - Lupa memberikan Attribut nama

                                                --}}
                                              <input type="file" name="foto" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />
                                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                          </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col mt-3">
                                        <label for="">Kompetensi Keahlian</label>
                                        <select name="kk" class="form-control">
                                            <option value="1">RPL</option>
                                        </select>
                                    </div>
                                </div>
                                <h4 class="mb-4 mt-5 font-weight-bold">Akun Murid</h4>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Username</label>
                                        <input type="text" name="username" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Email</label>
                                        <input type="email" name="email" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Password</label>
                                        <input type="password" name="password" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col mt-3">
                                        <button type="submit" class="btn btn-success btn-block btn-lg">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
