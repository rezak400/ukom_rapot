@extends('layout/sidebar')

@section('isi')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pt-4">
                <div class="content bg-white rounded" style=" height: 115vh;">
                    <div class="row">
                        <div class="col-12 pl-5 py-4 " >
                            <h3 class="fa fa-users d-inline-block"></h3>&nbsp;<h3 class="d-inline-block font-weight-bold">Edit Murid</h3>
                        </div>
                    </div>
                    <div class="row px-5">
                        <div class="col">
                            <form action="{{ url('/siswa/update/'.$siswa->nisn) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Nama</label>
                                        <input type="text" name="nama" value="{{ $siswa->siswa_nama }}" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Alamat</label>
                                        <input type="text" name="alamat" value="{{ $siswa->siswa_alamat }}" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" value="{{ $siswa->siswa_tanggal_lahir }}" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Kelas</label>
                                        <input type="text" name="kelas" value="{{ $siswa->siswa_kelas }}" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">NISN</label>
                                        <input type="text" name="nisn" value="{{ $siswa->nisn }}" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Telpon</label>
                                        <input type="text" name="telpon" value="{{ $siswa->siswa_telpon }}" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                          <label for="">Foto</label>
                                          <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                              {{--

                                                <input type="file" name="foto" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />

                                                Debugging - Lupa memberikan Attribut nama

                                                --}}
                                              <input type="file" name="foto" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />
                                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                          </div>
                                    </div>
                                    <div class="col">
                                        <label for="">Preview</label>
                                        <br>
                                        <img src="{{ asset('uploads/'.$siswa->siswa_foto) }}" width="40" alt="">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Kompetensi Keahlian</label>
                                        <select name="kk" class="form-control">
                                            <option >TKJ</option>
                                            <option value="1" @if($siswa->id_kk == '1') selected @endif>RPL</option>
                                        </select>
                                    </div>
                                </div>
                                <h4 class="mb-4 mt-5 font-weight-bold">Akun Murid</h4>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="">Username</label>
                                        <input type="text" name="username" disabled value="{{$siswa->user->username}}" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Email</label>
                                        <input type="email" name="email" disabled value="{{$siswa->user->email}}" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="">Password</label>
                                        <input type="password" name="password" disabled value="{{$siswa->user->password}}" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col mt-3">
                                        <button type="submit" class="btn btn-success btn-block btn-lg">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
