@extends('layout/sidebar')
@section('')

@section('isi')
    <div class="container-fluid">
        <div class="row pt-4 pb-3">
            <div class="col">
                <h3 class="font-weight-bold"><i class="fa fa-users"></i>&nbsp; Data Siswa</h3>
            </div>
        </div>
        <div class="row pb-4">
            <div class="col">
                {{--

                    <form action="{{ url('/siswa') }}" method="POST" class="form-inline">
                        <input class= "form-control w-75 mr-3" name="cari" type="text" placeholder="Search...">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </form>

                    Debugging - Lupa menempatkan csrf field

                    lupa menambahkan @csrf field sehingga pencarian data memunculkan error "page expired"

                    By Abu

                --}}
                <form action="{{ url('/siswa') }}" method="POST" class="form-inline">
                    @csrf
                    <input class= "form-control w-75 mr-3" name="cari" type="text" placeholder="Search...">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="col-2">
                <select class="form-control d-inline-block" name="sorting" onchange="window.location.href = '/siswa/'+value">
                    <option value="asc" @if(Request::url(2) == 'http://127.0.0.1:3000/siswa/asc') selected @endif>Dari A-Z</option>
                    <option value="desc" @if(Request::url(2) == 'http://127.0.0.1:3000/siswa/desc') selected @endif>Dari Z-A</option>
                </select>
            </div>
            <div class="col">
                <a href="{{ url('/siswa/create')}}" class="btn btn-block btn-success font-weight-bold w-50 float-right">Tambah <i class="fa fa-plus-circle"></i></a>
            </div>
        </div>
        <div class="row px-3">
            @if(session()->has('pesan'))
                <div class="alert alert-info w-100">
                    {{ session('pesan') }}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped text-center">
                    <thead class="thead-dark">
                      <tr>
                        <th>No</th>
                        <th scope="col">NISN</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Telpon</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($siswas as $siswa)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <th>{{$siswa->nisn}}</th>
                                <td>{{$siswa->siswa_nama}}</td>
                                <td>{{$siswa->siswa_alamat}}</td>
                                <td>{{$siswa->siswa_kelas}}</td>
                                <td>{{$siswa->siswa_telpon}}</td>
                                <td>
                                    <img src="{{ asset('uploads/'.$siswa->siswa_foto) }}" width="38" alt="">
                                </td>
                                <td>
                                    <a class="btn btn-primary" href="{{ url('/siswa/edit/'.$siswa->nisn) }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('yakin dihapus?')" href="{{ url('/siswa/destroy/'.$siswa->nisn) }}"><i class="fa fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
