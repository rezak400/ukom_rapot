@extends('layout.auth')
@section('title', 'Login | App Rapot')

@section('content')
    <div class="box-login mx-auto rounded" @error('email') style="padding-top: 14.5vh;" @enderror>
        <h3 class="text-center text-white font-weight-bold">LOGIN ACCOUNT</h3>
        <img src="{{ asset('img/user-auth.png') }}" class="d-block mt-4 mb-4 pb-3 mx-auto" width="130" alt="">
        <form action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group">
                <input type="text" name="email" class="form-control w-75 mx-auto @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" autocomplete="email">
                @error('email')
                    <small class="text-danger m-0 p-0 pt-1 w-75 d-block mx-auto">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control @if($errors->has('password')) mt-2 @else mt-4 @endif w-75 mx-auto @error('password') is-invalid @enderror" placeholder="Password" autocomplete="current-password">
                @error('password')
                    <small class="text-danger m-0 p-0 pt-1 w-75 d-block mx-auto">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember" value="true" checked hidden>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-color btn-block @error('password') mt-2 @enderror mx-auto font-weight-bold w-75">SIGN IN</button>
            </div>
        </form>
        <center>
            <a href="{{ url('/reset') }}" class="text-white link text-center">Lupa Password?</a>
        </center>
    </div>
@endsection
