<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ asset("vendor/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{ asset("vendor/font-awesome/font-awesome/css/all.css")}}">
    <link rel="stylesheet" href="{{ asset("css/auth.css")}}">

    <style>
        body {
            background-color: #432fc2;
        }
        input {
            border-radius: 50px;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4">

            </div>
            <div class="col">
                <form action="{{ route('postLogin') }}" class="form-signin text-center" action="{{ url("/login")}}" method="post">
                    @csrf

                    <div style="margin-top: 110px; color:white" class="logo">
                        <h1 class="font-weight-bold">LOGIN ACCOUNT</h1>
                    </div>
                    <div>
                        <p style="font-size: 150px">
                            <i class="fa fa-users text-white"></i>
                        </p>
                    </div>

                    <input type="text" class="form-control mt-3" placeholder="Username"  name="username">

                    <input type="password" class="form-control mt-3" placeholder="Password" name="password">

                    <button type="submit" class="btn btn-lg btn-light btn-block mt-5 font-weight-bold" type="submit">SIGN IN</button>
                </form>
            </div>
            <div class="col-4">

            </div>
        </div>
    </div>
</body>
</html>
