<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    // protected $primaryKey = 'nisn';
    // protected $keyTypes = 'string';
    protected $fillable = ['nisn', 'id_user', 'id_kk', 'siswa_nama', 'siswa_alamat', 'siswa_tanggal_lahir', 'siswa_foto', 'siswa_kelas', 'siswa_telpon'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
