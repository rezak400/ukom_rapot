<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    //
    protected $fillable = ["guru_nama","guru_nip","guru_alamat","guru_telpon","guru_foto","id_user"];
}
