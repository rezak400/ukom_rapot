<?php

namespace App\Http\Controllers;

use App\BidangStudi;
use Illuminate\Http\Request;

class BidangStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BidangStudi  $bidangStudi
     * @return \Illuminate\Http\Response
     */
    public function show(BidangStudi $bidangStudi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BidangStudi  $bidangStudi
     * @return \Illuminate\Http\Response
     */
    public function edit(BidangStudi $bidangStudi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BidangStudi  $bidangStudi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BidangStudi $bidangStudi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BidangStudi  $bidangStudi
     * @return \Illuminate\Http\Response
     */
    public function destroy(BidangStudi $bidangStudi)
    {
        //
    }
}
