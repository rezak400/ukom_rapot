<?php

namespace App\Http\Controllers;

use App\KompetensiDasar;
use Illuminate\Http\Request;

class KompetensiDasarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KompetensiDasar  $kompetensiDasar
     * @return \Illuminate\Http\Response
     */
    public function show(KompetensiDasar $kompetensiDasar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KompetensiDasar  $kompetensiDasar
     * @return \Illuminate\Http\Response
     */
    public function edit(KompetensiDasar $kompetensiDasar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KompetensiDasar  $kompetensiDasar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KompetensiDasar $kompetensiDasar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KompetensiDasar  $kompetensiDasar
     * @return \Illuminate\Http\Response
     */
    public function destroy(KompetensiDasar $kompetensiDasar)
    {
        //
    }
}
