<?php

namespace App\Http\Controllers;

use App\GuruPelajaran;
use Illuminate\Http\Request;

class GuruPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GuruPelajaran  $guruPelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(GuruPelajaran $guruPelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GuruPelajaran  $guruPelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(GuruPelajaran $guruPelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GuruPelajaran  $guruPelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GuruPelajaran $guruPelajaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GuruPelajaran  $guruPelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(GuruPelajaran $guruPelajaran)
    {
        //
    }
}
