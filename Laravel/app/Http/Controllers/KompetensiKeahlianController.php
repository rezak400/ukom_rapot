<?php

namespace App\Http\Controllers;

use App\KompetensiKeahlian;
use Illuminate\Http\Request;

class KompetensiKeahlianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KompetensiKeahlian  $kompetensiKeahlian
     * @return \Illuminate\Http\Response
     */
    public function show(KompetensiKeahlian $kompetensiKeahlian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KompetensiKeahlian  $kompetensiKeahlian
     * @return \Illuminate\Http\Response
     */
    public function edit(KompetensiKeahlian $kompetensiKeahlian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KompetensiKeahlian  $kompetensiKeahlian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KompetensiKeahlian $kompetensiKeahlian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KompetensiKeahlian  $kompetensiKeahlian
     * @return \Illuminate\Http\Response
     */
    public function destroy(KompetensiKeahlian $kompetensiKeahlian)
    {
        //
    }
}
