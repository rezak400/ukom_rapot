<?php

namespace App\Http\Controllers;

use App\Guru;
use App\User;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->has("search")) {
            $query = Guru::where("guru_nama","LIKE","%".$request->search."%")->get();            
        }else{
            $query = Guru::all();
        }

        return view("guru/index", compact("query"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $id = User::get()->where();
        // $user = User::create([
        //     "id_role" => 
        // ])

        // $getIdUser = $user->id;
        $user = new User;

        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->id_role = 2;
        $user->save();

        $getIdUser = $user->id;

        $guru = new Guru;
        $guru->id_user      = $getIdUser;
        $guru->guru_nama    = $request->guru_nama;
        $guru->guru_nip     = $request->guru_nip;
        $guru->guru_alamat  = $request->guru_alamat;
        $guru->guru_telpon  = $request->guru_telpon;
        if($request->has('file')) {
            $request->file('file')->move(public_path().'/uploads', $request->file('file')->getClientOriginalName());
            $guru->guru_foto = $request->file('file')->getClientOriginalName();
        }
        // dd($request->has("file"));
        // dd($request->file("file"));
        // $guru->guru_foto    =$request->file;
        $guru->save();

        return redirect()->back()->with("alert","berhasil");
        // return $getIdUser;        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function show(Guru $guru)
    {
        //
        $query = $guru;
        // dd($query);
        $userid = $guru->id_user;
        // dd(User::find($userid));
        $userguru = User::find($userid);
        // dd(
        // $userid = $guru->id_user
        // );
        return view("/guru/edit", compact("query","userguru"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function edit(Guru $guru)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guru  $guru
     * @return \Illuminate\Http\Response
     */

     // UPDATE TABLE GURU
    public function update(Request $request, Guru $guru)
    {
        //
        // dd($guru->id);

        // INI SALAH
        // $request->file('file')->move(public_path().'/uploads', $request->file('file')->getClientOriginalName());
        // $guru->guru_foto = $request->file('file')->getClientOriginalName();
        
        // INI BENER
        Guru::find($request->id)->update($request->all());
        Guru::find($request->id)->update([
            "guru_foto" => $request->file("file")->getClientOriginalName()
        ]);
        // dd($guru);
        // dd($request->all());
        // dd($request->id);
        // dd(Guru::find($request->id));
        return redirect()->back()->with("message","berhasil");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guru $guru,$id)
    {
        //
        // $guru = Guru::find($guru);
        // $guru->delete();

        // return redirect()->back()->with("alert","berhasil");
        $hapus = Guru::find($id);
        $hapus->delete();

        return redirect()->back();
    }

    public function sorting($sort)
    {
        $query = Guru::orderBy('guru_nama', $sort)->get();
        return view('guru/index', compact('query'));
    }
}
