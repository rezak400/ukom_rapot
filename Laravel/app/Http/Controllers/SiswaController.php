<?php

namespace App\Http\Controllers;

use App\User;
use App\Siswa;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('cari')) {
            $siswas = Siswa::where('siswa_nama', 'LIKE', '%' . $request->cari . '%')
                ->orWhere('siswa_alamat', 'LIKE', '%' . $request->cari . '%')
                ->orWhere('nisn', 'LIKE', '%' . $request->cari . '%')
                ->orWhere('siswa_kelas', 'LIKE', '%' . $request->cari . '%')
                ->get();
        } else {
            $siswas = Siswa::all();
        }

        return view('siswa.index', compact('siswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;

        /**
         *
         * Debugging - lupa menambahkan role
         *
         * lupa menambahkan role 3 sebagai siswa sehingga
         * memunculkan pesan error "field doesn't match blabla....."
         *
         *
         */

        $user->id_role = 3;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();

        $siswa = new Siswa;

        $siswa->nisn = $request->nisn;
        $siswa->id_user = $user->id;
        $siswa->id_kk = $request->kk;
        $siswa->siswa_nama = $request->nama;
        $siswa->siswa_alamat = $request->alamat;
        $siswa->siswa_tanggal_lahir = $request->tanggal_lahir;
        $siswa->siswa_kelas = $request->kelas;
        $siswa->siswa_telpon = $request->telpon;

        if ($request->has('foto')) {
            $request->file('foto')->move(public_path() . '/uploads', $request->file('foto')->getClientOriginalName());
            $siswa->siswa_foto = $request->file('foto')->getClientOriginalName();
        }

        // if($request->has('foto')) {
        //     $request->file('foto')->move(public_path().'uploads/', $request->file()->getClientOriginalName());
        //     $siswa->siswa_foto = $request->file()->getClientOriginalName();
        // }

        /**
         *
         * Debugging - Lupa memberikan argumen
         *
         * lupa memberikan argumen 'foto' pada $request->file
         * sehingga foto tidak terupload.
         *
         */


        $siswa->save();

        return redirect('/siswa')->with('pesan', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa, $nisn)
    {
        $siswa =  Siswa::where('nisn', $nisn)->first();

        // $siswa =  Siswa::findOrFail();
        /**
         *
         * Debugging - Lupa memberikan argumen
         *
         * lupa memberikan argumen pada method findOrFail
         * untuk mencari siswa berdasarkan id sehingga memunculkan error
         * "Too few arguments to function....."
         *
         */

        return view('siswa.edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa, $nisn)
    {
        // $siswa = Siswa::find($nisn);
        /**
         *
         * Debugging - pemberian method
         *
         * karena primary key tabel siswa bukan id
         * maka metod untuk mencari siswa berdasarkan id
         * maka menggunakan where() saja
         *
         */

        if ($request->has('foto')) {

            Siswa::where('nisn', $nisn)->update([
                'nisn' => $request->nisn,
                'id_kk' => $request->kk,
                'siswa_nama' => $request->nama,
                'siswa_alamat' => $request->alamat,
                'siswa_tanggal_lahir' => $request->tanggal_lahir,
                'siswa_kelas' => $request->kelas,
                'siswa_telpon' => $request->telpon,
                'siswa_foto' => $request->file('foto')->getClientOriginalName(),
            ]);

            $request->file('foto')->move(public_path() . '/uploads', $request->file('foto')->getClientOriginalName());
        } else {

            Siswa::where('nisn', $nisn)->update([
                'nisn' => $request->nisn,
                'id_kk' => $request->kk,
                'siswa_nama' => $request->nama,
                'siswa_alamat' => $request->alamat,
                'siswa_tanggal_lahir' => $request->tanggal_lahir,
                'siswa_kelas' => $request->kelas,
                'siswa_telpon' => $request->telpon,
            ]);
        }

        return redirect('/siswa')->with('pesan', 'Data Berhasil Diubah');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa, $nisn)
    {
        Siswa::where('nisn', $nisn)->delete();

        return redirect()->back()->with('pesan', 'Data Berhasil Dihapus');
    }

    public function sorting($sort)
    {
        $siswas = Siswa::orderBy('siswa_nama', $sort)->get();

        return view('siswa.index', compact('siswas'));
    }
}
